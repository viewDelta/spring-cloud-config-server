# Read Me First
The following was discovered as part of building this project:

* The original package name 'com.red.ct.spring-cloud-config-server' is invalid and this project uses 'com.red.ct.springcloudconfigserver' instead.

# Prerequisite
Need to have a codecommit url which it should point to and a EC2 or ECS instance through which other application can access the configuration from AWS 

# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.6.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.6.RELEASE/maven-plugin/reference/html/#build-image)

### Accessing Profile
http://localhost:8888/<file-name>/<profile>

### Guides
The following guides illustrate how to use some features concretely:

* [Centralized Configuration](https://spring.io/guides/gs/centralized-configuration/)

